#!/usr/bin/env python

import argparse
import numpy as np
import sys
import os
import errno
import re
import caffe
import pickle

def set_temperature(fpath_design, temperature):
    assert temperature > 0

    found = False
    new_lines = []

    with open(fpath_design, 'r') as f:
        for line in f:
            new_line, n_replaced = re.subn(
                r'^(\s*value: )([0-9\.]+)( # TEMPERATURE.*)$',
                r'\g<1>%.10f\g<3>' % (1.0 / temperature),
                line)
            new_lines.append(new_line)

            if n_replaced > 0:
                if found:
                    sys.exit("ERROR: multiple places for temperature found in prototxt file.")
                found = True

    if not found:
        sys.exit("ERROR: no place for temperature found in prototxt file.")

    with open(fpath_design, 'w') as f:
        f.writelines(new_lines)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno != errno.EEXIST or not os.path.isdir(path):
            raise

def list_images(directory):
    """Return a recursive list of the paths of regular files in `directory`,
    relative to this directory."""

    result = []
    for (dirpath, dirnames, filenames) in os.walk(directory):
        for f in filenames:
            result.append(os.path.relpath(os.path.join(dirpath, f), directory))
    return result

def classify_images(fpath_design, fpath_weights, fpath_mean, images_path, output_path, gpu_number):
    caffe.set_device(gpu_number)
    caffe.set_mode_gpu()

    net = caffe.Net(fpath_design, fpath_weights, caffe.TEST)

    # Preprocess input to match the format expected by the network.
    # Images will be automatically rescaled to match the shape of the input in the constructor.
    # For details see: <https://github.com/BVLC/caffe/blob/master/python/caffe/io.py>.
    transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
    transformer.set_transpose('data', (2, 0, 1)) # Change HxWxC into CxHxW?
    transformer.set_channel_swap('data', (2, 1, 0)) # Change RGB to BGR?
    transformer.set_raw_scale('data', 255.0) # Python represents colors in [0, 1], model in [0, 255].
    transformer.set_mean('data', np.load(fpath_mean).mean(1).mean(1)) # Subtract per-channel means of the ImageNet dataset.

    # Since we classify only one image, we change batch size (first parameter) from 10 to 1.
    net.blobs['data'].reshape(1, 3, 224, 224)

    # Classify all images in images_path.
    for file in list_images(images_path):
        im = caffe.io.load_image(os.path.join(images_path, file))
        net.blobs['data'].data[...] = transformer.preprocess('data', im)
        out = net.forward()
        assert len(out['prob'][0]) == 365  # There are 365 classes in Places365-Standard.

        if output_path is None:
            print(out)
        else:
            filename = os.path.splitext(file)[0] + '.npy'
            out_path = os.path.join(output_path, filename)

            dirname = os.path.dirname(out_path)
            if dirname:
                mkdir_p(dirname)

            np.save(out_path, out['prob'][0])

            print(out_path)

        # Print top 5 labels. (May be useful for debugging later.)
        # fpath_labels = 'resources/labels.pkl'
        # with open(fpath_labels, 'rb') as f:
        #     labels = pickle.load(f)
        #     top_k = net.blobs['prob'].data[0].flatten().argsort()[-1:-6:-1]
        #     for i, k in enumerate(top_k):
        #         print i, labels[k]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Saves the large network\'s outputs for distillation')
    parser.add_argument('--images-path', type=str, help='path to directory with images', required=True)
    parser.add_argument('--output-path', type=str, help='path where to save the the network\'s outputs')
    parser.add_argument('--temperature', type=float, help='temperature to use in the softmax', required=True)
    args = parser.parse_args()

    resources_path = os.environ['RESOURCES']
    fpath_design = 'deploy_vgg16_places365.prototxt'
    fpath_weights = os.path.join(resources_path, 'vgg16_places365.caffemodel')
    fpath_mean = os.path.join(resources_path, 'ilsvrc_2012_mean.npy')

    set_temperature(fpath_design, args.temperature)
    classify_images(fpath_design, fpath_weights, fpath_mean, args.images_path, args.output_path, 0)
