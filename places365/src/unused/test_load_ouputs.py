#!/usr/bin/env python
"""
Test for loading form files distlled output of network 
"""
import numpy as np
import os


if __name__ == '__main__':
    output_path = os.environ['OUTDIR']
    for file in os.listdir(output_path):
        abs_path = os.path.join(output_path, file)
        arr = np.load(abs_path)
        print('File: %s' % file)
        print('Array: %s' % str(arr.shape))
        assert arr.shape[0] == 365
        # print(arr)
