#!/usr/bin/env python

"""
The only purpose of this file is to test softmax with temperature layer implemented in caffe.
"""
import numpy as np
import sys
import os
import caffe
import pickle
import math

TEMPERATURE = 10
EPSILON = 0.01

def init_net(fpath_design, fpath_weights, fpath_mean):
    # init caffe
    caffe.set_device(0)
    caffe.set_mode_gpu()

    # initialize net
    net = caffe.Net(fpath_design, fpath_weights, caffe.TEST)

    # load input and configure preprocessing
    transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
    # TODO figure out why this is here and if it's needed:
    transformer.set_mean('data', np.load(fpath_mean).mean(1).mean(1))
    transformer.set_transpose('data', (2,0,1))
    transformer.set_channel_swap('data', (2,1,0))
    transformer.set_raw_scale('data', 255.0)

    # since we classify only one image, we change batch size from 10 to 1
    net.blobs['data'].reshape(1,3,224,224)

    return net, transformer


def get_output(net, end_layer):
    # compute; https://github.com/BVLC/caffe/issues/1146
    out = net.forward(end=end_layer)
    return out[end_layer][0]


def compute_softmax_with_temp(values):
    out = np.zeros(len(values))
    total = 0.0

    for i, v in enumerate(values):
        tmp = math.exp(v / TEMPERATURE)
        total += tmp
        out[i] = tmp
    out /= total
    return out


def compare_outputs(net):
    sotmax_out = get_output(net, 'prob')
    fc8a_out = get_output(net, 'fc8a')

    target = compute_softmax_with_temp(fc8a_out)
    out = np.asarray(sotmax_out)

    max_error = np.amax(np.abs(out - target))
    assert max_error < EPSILON


if __name__ == '__main__':
    resources_path=os.environ['RESOURCES']
    dataset_path=os.environ['DATASET']

    # fetch pretrained models
    fpath_design = 'deploy_vgg16_places365.prototxt'
    fpath_weights = os.path.join(resources_path, 'vgg16_places365.caffemodel')
    fpath_mean = os.path.join(resources_path, 'ilsvrc_2012_mean.npy')


    net, transformer = init_net(fpath_design, fpath_weights, fpath_mean)

    # compare outputs for all images in dataset dir
    for file in os.listdir(dataset_path):
        # fetch image
        im = caffe.io.load_image(os.path.join(dataset_path, file))
        # load the image in the data layer
        net.blobs['data'].data[...] = transformer.preprocess('data', im)
        compare_outputs(net)
    print('Softmax with temperature ok')
