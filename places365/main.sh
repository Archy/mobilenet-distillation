#!/usr/bin/env bash

image_name="places365:latest"
this_script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

dataset_path="$this_script_dir/../../Places365"
output_path="$this_script_dir/../../Distilled"

src_path="$this_script_dir/src"
resources="$this_script_dir/resources"

error() {
    printf "ERROR: %s\\n" "$1" 1>&2
}

fatal_error() {
    error "$1"
    exit 1
}

if [ "$EUID" -ne 0 ]; then
    fatal_error "This script must be run with root privileges."
fi

get_missing_resource() {
    if [ ! -f "$resources/$1" ]; then
        echo "$1 not found. Downloading."
        wget -P "$resources" -- "$2"
    fi
}

# download needed resources files, if they're missing
mkdir -p -- "$resources"
get_missing_resource vgg16_places365.caffemodel "http://places2.csail.mit.edu/models_places365/vgg16_places365.caffemodel"
get_missing_resource ilsvrc_2012_mean.npy "https://github.com/BVLC/caffe/raw/master/python/caffe/imagenet/ilsvrc_2012_mean.npy"

docker build -t "$image_name" .
docker run --runtime=nvidia \
       -v "$src_path:/root/workdir" \
       -v "$dataset_path:/root/dataset" \
       -v "$output_path:/root/output" \
       -v "$resources:/root/resources" \
       -it "$image_name" "$@"
