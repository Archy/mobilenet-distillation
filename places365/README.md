## VGG16-places365 in Docker
This is Docker image for running VGG16-places365 caffe model on GPU.
It's based on https://github.com/CSAILVision/places365/blob/master/docker.

NOTE that original softmax layer has been already removed from deploy_vgg16_places365.prototxt

## Softmax with temperature layer
Taken from: https://stackoverflow.com/questions/33622333/caffe-softmax-with-temperature
To replace temperature just change the *0.1* in `src/deploy_vgg16_places365.prototxt`:
```
    filler: { type: 'constant' value: 0.1 }
```
to your own value of 1/Temperature.


## Build & Run
* adjust paths in main.sh
* run container and prediction inside it:
    ```
    sudo ./main.sh bash -c 'ldconfig && temp=10 && for dir in test_256 val_256 data_256; do ./run_scene.py --temperature="$temp" --images-path="$DATASET/$dir" --output-path="$OUTDIR/$temp/$dir" || break; done'
    ```
    It will save the predictions for all images Places365.


## Random notes:
* AlexNet and VGG require the input image scale to be from 0-255. Not sure about others, but they mention that some requires 0-1 scale.
