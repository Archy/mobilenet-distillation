## mobilenet directory
Things related to training mobilenet using distillation (on already existing data set)

## places365 directory
Things related to creating data set for the distillation (outputs from bigger network)

## Dataset
We're using the Places-365 dataset. To get it, go to <http://places2.csail.mit.edu/download.html>. Download the "Image list of train and val for Places365-Standard" from the "Places365 Development kit" section and train, validation and test images from the "Small images (256 * 256)" section. Extract everything in your dataset directory. By default, the scripts assume that this directory is `../Places365` (relative to this README) and the large network's outputs are in `../Distilled`.

## TODO
### Mobilenet
- [x] Implement loss
- [x] Normalize loaded image
- [x] Add proper callback to model.fit_generator
- [x] Loading network weights from file (for resuming training)
- [x] Model evaluation
- [x] Train distilled model to produce the correct labels as well (as described in original paper)
- [x] Use proper loss function
- [x] Shuffle data before each epoch
- [x] Add validation during training (check predicted class   vs   compare output with bigger network output) --check if possible.
- [x] Fix DistilledDataGenerator to work with actual train data (images are in some subdirectories)
- [x] Make data generator robust - huge bottleneck right now
- [x] Train network

### Places365
- [x] Wget all data from CSAILVision rather than keep them in the repository
- [x] Mount Places365-Standard images directory as volume and use it
- [x] Figure out if ilsvrc_2012_mean should be still used; it was originaly used with alexnet, so maybe it's not valid for vgg  
    -> we should normalize by subtracting a mean, but it probably should be the mean of our dataset, not the ImageNet one  
    -> based on [this](https://github.com/CSAILVision/places365/issues/9) and several other discussions there I understand that they've actually used ImageNet mean. Also based on [this](https://github.com/CSAILVision/places365/issues/3) I assume that this actually doesn't matter.
- [x] Decide how to save output for mobilenet training and do it
- [x] Choose temperature  
    -> Paweł's suggestion: try 2, 1, 5, 10 (in that order, as time permits)

## Train results
* The data set has been limited to 1000 examples per class -> 365,000 samples in total
* Full validation dataset has been used -> 36,500 samples

### Preliminary results (10 epochs each)
| Temperature | Train accuracy | Validation accuracy | Output data link |
| ----------- | -------------- | ------------------- | ---------------- |
| 5  | 0.5332 | 0.4617 | [Google Drive](https://drive.google.com/drive/folders/1zgbItc4MOTh7I93UgGmHRFDo_VRf2jyt?usp=sharing) |
| 10 | 0.5568 | 0.5574 | [Google Drive](https://drive.google.com/drive/folders/18UOa6KkW8CegQ3BcOwPB005vK6RBKdut?usp=sharing) |

### Main results (15 epochs each)
For full results, see the CSV files in the `results/1000 images for each class` directory.

| Temperature | Train acc (last) | Val acc (last) | Train acc (last 3 avg) | Val acc (last 3 avg) |
| ----------- | ---------------- | -------------- | ---------------------- | -------------------- |
| No distillation | 0.6556 | 0.3906 | 0.6357 | 0.3937 |
| Temperature 1   | 0.6396 | 0.4550 | 0.6273 | 0.4528 |
| Temperature 2   | 0.6193 | 0.4675 | 0.6079 | 0.4680 |
| Temperature 5   | 0.5955 | 0.4681 | 0.5843 | 0.4689 |
| Temperature 10  | 0.6108 | 0.4705 | 0.5985 | 0.4682 |

### Full dataset results (15 epochs each)
For full results, see the `results/Full dataset` directory.

| Temperature | Train accuracy | Validation accuracy |
| ----------- | -------------- | ------------------- |
| Temperature 1  | 0.5904 | 0.5167 |
| Temperature 10 | 0.5787 | 0.5251 |

## Useful links & commands
* Monitor gpu usage: watch -n 0.5 nvidia-smi
* <http://christopher5106.github.io/deep/learning/2015/09/04/Deep-learning-tutorial-on-Caffe-Technology.html>
* <https://github.com/nitnelave/pycaffe_tutorial/blob/master/01%20Loading%20a%20network.ipynb>
