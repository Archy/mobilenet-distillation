import numpy as np
import unittest
from src.model import Mobilenet

num_classes = 365

expected_shapes = np.array([
    [224, 224, 3],

    [112, 112, 32],
    [112, 112, 32],
    [112, 112, 64],

    [56, 56, 64],
    [56, 56, 128],
    [56, 56, 128],
    [56, 56, 128],

    [28, 28, 128],
    [28, 28, 256],
    [28, 28, 256],
    [28, 28, 256],

    [14, 14, 256],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],
    [14, 14, 512],

    [7, 7, 512],
    [7, 7, 1024],
    [7, 7, 1024],
    [7, 7, 1024],

    [1024],
    [num_classes],  # fully connected layer
    [num_classes],  # hard predictions (normal softamx)
    [num_classes * 2],  # output of model for training
])


class MobilenetModelTest(unittest.TestCase):
    def test_print_layers_shapes(self):
        model = Mobilenet(num_classes, 10)
        model.compile()
        model.model.summary()

        shapes = model.get_layers_outputs()
        self.assertTrue(np.array_equal(shapes, expected_shapes))
