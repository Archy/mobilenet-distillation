import time
import unittest
import numpy as np
import cv2

from src.data_generator import DistilledDataGenerator, MEAN, LabeledDataGenerator

DATASET_PATH = r'D:\Datasets\Places-365'
IMG_PATH = r'C:\Users\Jan\Desktop\mobilenet-distillation\places365\images\Places365_test_00000001.jpg'

VAL_PATH = r'D:\Datasets\Places-365\val_256'
VAL_LIST_PATH = r'D:\Datasets\Places-365\places365_val.txt'


def _load_img():
    img = cv2.imread(IMG_PATH)
    if img.shape != (224, 224, 3):
        img = cv2.resize(img, (224, 224))
    return np.array(img)


def create_data_generator(batch_size):
    return DistilledDataGenerator(img_w=224, img_h=224, batch_size=batch_size, dataset_path=DATASET_PATH, num_classes=365)
    return LabeledDataGenerator(img_w=224,
                                img_h=224,
                                batch_size=batch_size,
                                subset_name='labeled',
                                images_path=VAL_PATH,
                                images_list_path=VAL_LIST_PATH,
                                num_classes=365, mock_distilled=True)


class DataGeneratorTest(unittest.TestCase):
    def test_generator(self):
        batch_size = 10
        data_generator = create_data_generator(batch_size)
        generator = data_generator.generator()

        x, y = next(generator)
        self.assertEqual(x.shape, (batch_size, 224, 224, 3))
        self.assertEqual(y.shape, (batch_size, 365 * 2))
        # self.assertEqual(y.shape, (batch_size, 365))

        self.assertEqual(data_generator.count_steps_per_epoch(), 3650)

    def test_normalization(self):
        data_generator = create_data_generator(1)

        normalized = data_generator._load_img(IMG_PATH)
        original = _load_img()

        r, g, b = MEAN
        for x in range(224):
            for y in range(224):
                normalized_pixel = normalized[x][y]
                pixel = original[x][y]
                self.assertAlmostEqual(pixel[0] - r, normalized_pixel[0])
                self.assertAlmostEqual(pixel[1] - g, normalized_pixel[1])
                self.assertAlmostEqual(pixel[2] - b, normalized_pixel[2])

    def test_time(self):
        data_generator = create_data_generator(25)
        generator = data_generator.generator()

        epochs = 2
        batches_per_epoch = data_generator.count_steps_per_epoch()

        print('Testing time')
        print("%d epoch with %d batches" % (epochs, batches_per_epoch), flush=True)

        start = time.time()
        for e in range(epochs):
            for b in range(batches_per_epoch):
                next(generator)
            print("Epoch %d finished in %f" % (e, time.time() - start), flush=True)
        end = time.time()
        print('Took: %f seconds' % (end - start))
