#!/usr/bin/env bash

this_script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
dataset_path="$this_script_dir/../.."
src_path="$this_script_dir"
image_name="mobilenet:latest"

error() {
    printf "ERROR: %s\\n" "$1" 1>&2
}

fatal_error() {
    error "$1"
    exit 1
}

if [ "$EUID" -ne 0 ]; then
    fatal_error "This script must be run with root privileges."
fi

# If we got arguments, use them as command to execute inside the container.
if [ $# -gt 0 ]; then
    command=("$@")
else
    command=("/bin/bash")
fi

docker build -t "$image_name" .
docker run --runtime=nvidia \
       -v "$src_path:/root/workdir" \
       -v "$dataset_path:/root/dataset" \
       -it "$image_name" "${command[@]}"
