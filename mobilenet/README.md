# Training Mobilenet from another network's output
Tool for training mobilenet on places365 using network distillation.

## Dataset directory structure
The train.py script expects the following directory structure:
```
.
├── train_256  # directory with train images. Note that only actual train images must be present there.
|   ├── Places365_???_00000001.jpg
|   ├── Places365_???_00000002.jpg
|   ...
├── train_256_distilled  # outputs obtained from bigger network for train data set
|   ├── Places365_???_00000001.npy
|   ├── Places365_???_00000002.npy
|   ...
├── places365_train_standard.txt
|
├── val_256
|   ├── Places365_val_00000001.jpg
|   ├── Places365_val_00000002.jpg
|   ...
├── places365_val.txt
```
The files lists can be downloaded from: http://data.csail.mit.edu/places/places365/filelist_places365-standard.tar

## Build and run
* adjust paths in main.sh
* run training:
    ```
    sudo ./main.sh bash -c 'ldconfig && temp=1 && ./src/train.py --distilled-path="$DATASET/Distilled/$temp/data_256" --temperature="$temp" --list-path="$DATASET/Places365/places365_train_standard.txt" --images-path="$DATASET/Places365/data_256" --val-list-path="$DATASET/Places365/places365_val.txt" --val-images-path="$DATASET/Places365/val_256" --batch=25 --out=out'
    ```
**Note that batch size must be a divisor of the number of cases in data set!**

## Random notes:
* loss is very small from very beginning and decreases almost unnoticeably: from *0.0206* to *0.0197* in 10 epochs. Hovewer at the same time training accuracy has grown from *0.0190* to *0.4472*
