#!/usr/bin/env python3

import logging

from src.data_generator import LabeledDataGenerator
from src.model import Mobilenet
from argparse import ArgumentParser


NUM_CLASSES = 365


def evaluate(weights_path, images_path, images_list_path, batch_size, max_queue_size):
    model = Mobilenet(NUM_CLASSES, 1)
    generator = LabeledDataGenerator(img_w=model.img_width,
                                     img_h=model.img_height,
                                     batch_size=batch_size,
                                     subset_name='test',
                                     images_path=images_path,
                                     images_list_path=images_list_path,
                                     num_classes=NUM_CLASSES)
    model.compile()
    model.evaluation_model.load_weights(weights_path)

    evaluation = model.evaluation_model.evaluate_generator(
        generator=generator.generator(),
        steps=generator.count_steps_per_epoch(),
        max_queue_size=max_queue_size,
        verbose=1
    )
    print(evaluation)


if __name__ == "__main__":
    parser = ArgumentParser(description='Mobilenet training using network distillation')
    parser.add_argument('--weights_path', '-w', dest='weights_path', type=str, help='network weights path')
    parser.add_argument('--images_path', '-i', dest='images_path', type=str, help='data set absolute path')
    parser.add_argument('--images_list_path', '-l', dest='images_list_path', type=str, help='data set samples list absolute path')
    parser.add_argument('--batch', '-b', dest='batch_size', type=int, default=25, help='batch size')
    parser.add_argument('--max_queue_size', '-s', dest='max_queue_size', type=int, default=50, help='max size for the generator queue')
    config = parser.parse_args()
    logging.info(config)

    evaluate(config.weights_path, config.images_path, config.images_list_path, config.batch_size, config.max_queue_size)
