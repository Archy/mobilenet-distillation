#!/usr/bin/env python3

import logging
logging.basicConfig(filename='train.log', level=logging.DEBUG,
                    format='%(thread)-6d %(asctime)s %(levelname)-8s %(message)s')
import json
import os
from src.model import Mobilenet
from src.data_generator import DistilledDataGenerator, LabeledDataGenerator
from argparse import ArgumentParser
from keras.callbacks import ModelCheckpoint, CSVLogger


NUM_CLASSES = 365


def train_distilled(list_path,
                    images_path,
                    distilled_path,
                    val_list_path,
                    val_images_path,
                    epochs,
                    batch_size,
                    max_queue_size,
                    temperature,
                    output_path,
                    weights_path,
                    first_epoch):
    model = Mobilenet(NUM_CLASSES, temperature)
    generator = DistilledDataGenerator(img_w=model.img_width,
                                       img_h=model.img_height,
                                       batch_size=batch_size,
                                       num_classes=NUM_CLASSES,
                                       list_path=list_path,
                                       images_path=images_path,
                                       distilled_path=distilled_path)
    validation_generator = LabeledDataGenerator(img_w=model.img_width,
                                                img_h=model.img_height,
                                                batch_size=batch_size,
                                                subset_name='validation',
                                                list_path=val_list_path,
                                                images_path=val_images_path,
                                                num_classes=NUM_CLASSES,
                                                mock_distilled=True)
    model.compile()
    if weights_path is not None:
        logging.info('Loading weights from \'%s\' and starting from epoch %d', weights_path, first_epoch)
        model.model.load_weights(weights_path)

    history = model.model.fit_generator(
        generator=generator.generator(),
        steps_per_epoch=generator.count_steps_per_epoch(),
        epochs=epochs,
        verbose=2,
        callbacks=_prepare_callback(output_path),
        validation_data=validation_generator.generator(),
        validation_steps=validation_generator.count_steps_per_epoch(),
        max_queue_size=max_queue_size,
        initial_epoch=first_epoch
    )

    with open(os.path.join(output_path, 'history.json'), 'w') as f:
        json.dump(history.history, f)


def train_standard(list_path,
                   images_path,
                   val_list_path,
                   val_images_path,
                   epochs,
                   batch_size,
                   max_queue_size,
                   output_path,
                   weights_path,
                   first_epoch):
    model = Mobilenet(NUM_CLASSES, temperature=1)
    generator = LabeledDataGenerator(img_w=model.img_width,
                                     img_h=model.img_height,
                                     batch_size=batch_size,
                                     subset_name='train',
                                     list_path=list_path,
                                     images_path=images_path,
                                     num_classes=NUM_CLASSES,
                                     mock_distilled=False)
    validation_generator = LabeledDataGenerator(img_w=model.img_width,
                                                img_h=model.img_height,
                                                batch_size=batch_size,
                                                subset_name='validation',
                                                list_path=val_list_path,
                                                images_path=val_images_path,
                                                num_classes=NUM_CLASSES,
                                                mock_distilled=False)
    model.compile()
    if weights_path is not None:
        logging.info('Loading weights from \'%s\' and starting from epoch %d', weights_path, first_epoch)
        model.evaluation_model.load_weights(weights_path)

    history = model.evaluation_model.fit_generator(
        generator=generator.generator(),
        steps_per_epoch=generator.count_steps_per_epoch(),
        epochs=epochs,
        verbose=2,
        callbacks=_prepare_callback(output_path),
        validation_data=validation_generator.generator(),
        validation_steps=validation_generator.count_steps_per_epoch(),
        max_queue_size=max_queue_size,
        initial_epoch=first_epoch
    )

    with open(os.path.join(output_path, 'history.json'), 'w') as f:
        json.dump(history.history, f)


def _prepare_callback(output_path):
    checkpoints_dir = os.path.join(output_path, 'checkpoints')
    os.makedirs(checkpoints_dir, exist_ok=True)
    return [
        ModelCheckpoint(os.path.join(checkpoints_dir, 'weights{epoch:02d}.h5'), save_weights_only=True, mode='auto', period=1),
        CSVLogger(os.path.join(output_path, 'csvlog.log'), separator=',', append=False)
    ]


if __name__ == "__main__":

    parser = ArgumentParser(description='Mobilenet training using network distillation')

    # train type
    parser.add_argument('--no-distillation', '-n', action='store_true', default=False, help='if distillation should be used during training', required=False)

    # paths
    parser.add_argument('--list-path', '-l', type=str, help='path to image list file', required=True)
    parser.add_argument('--images-path', '-i', type=str, help='path to directory with images', required=True)
    parser.add_argument('--distilled-path', '-d', type=str, help='path to directory with teacher network\'s outputs', required=False)
    parser.add_argument('--val-list-path', '-vl', type=str, help='path to validation image list file', required=True)
    parser.add_argument('--val-images-path', '-vi', type=str, help='path to directory with validation images', required=True)
    parser.add_argument('--output', '-o', dest='output_path', type=str, help='path to output directory', required=True)

    # network stuff
    parser.add_argument('--temperature', '-t', type=int, help='softmax temperature; only for training', required=False)
    parser.add_argument('--epochs', '-e', type=int, default=2000, help='epochs')
    parser.add_argument('--batch', '-b', dest='batch_size', type=int, default=25, help='batch size')
    parser.add_argument('--max-queue-size', '-s', type=int, default=50, help='max size for the generator queue')

    # existing weights
    parser.add_argument('--weights-path', '-w', type=str, default=None, help='inital weights path')
    parser.add_argument('--first-epoch', '-f', type=int, default=0, help='first epoch nr')

    config = parser.parse_args()
    logging.info(config)

    if config.no_distillation:
        print('Training without distillation')
        train_standard(list_path=config.list_path,
                       images_path=config.images_path,
                       val_list_path=config.val_list_path,
                       val_images_path=config.val_images_path,
                       epochs=config.epochs,
                       batch_size=config.batch_size,
                       max_queue_size=config.max_queue_size,
                       output_path=config.output_path,
                       weights_path=config.weights_path,
                       first_epoch=config.first_epoch)
    else:
        print('Training with distillation')
        assert config.temperature
        train_distilled(list_path=config.list_path,
                        images_path=config.images_path,
                        distilled_path=config.distilled_path,
                        val_list_path=config.val_list_path,
                        val_images_path=config.val_images_path,
                        epochs=config.epochs,
                        batch_size=config.batch_size,
                        max_queue_size=config.max_queue_size,
                        temperature=config.temperature,
                        output_path=config.output_path,
                        weights_path=config.weights_path,
                        first_epoch=config.first_epoch)
