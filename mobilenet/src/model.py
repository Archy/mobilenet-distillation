from functools import partial
import numpy as np
from keras import Input, Model
from keras.layers import ZeroPadding2D, Conv2D, ReLU, BatchNormalization, DepthwiseConv2D, GlobalAveragePooling2D, \
    Dense, Lambda, Activation, concatenate
from keras.optimizers import Adam
from keras import backend as K
import tensorflow as tf


def _knowledge_distillation_loss(y_true, y_pred, teacher_multiplier, num_classes):
    """
    Weighted loss of 2 objective functions.
    teacher_multiplier should be Temperature^2 * (how many times more important the teacher's outputs are than the targets).
    """
    y_true_soft = y_true[:, num_classes:]
    y_true = y_true[:, :num_classes]

    y_pred_sotf = y_pred[:, num_classes:]
    y_pred = y_pred[:, :num_classes]

    return tf.losses.log_loss(y_true, y_pred) + teacher_multiplier * tf.losses.log_loss(y_true_soft, y_pred_sotf)


class Mobilenet:
    """
    Mobilenet implementation in keras prepared for transfer learning.
    There are 2 models defined that share (almost) the same layers:
    - model - used for training. The expected target tensor has shape: batch_size x (2*NUM_CLASSES).
                The reason for that is usage of 2 losses during training.
                The first NUM_CLASSES elements should be one-hot encoded actual target class.
                The remaining NUM_CLASSES elements should be distilled output of the teacher network.
    - evaluation_model - used for evaluation/predictions. Output of this model has size: batch_size x NUM_CLASSES and
                is the actual output of a regular softmax layer.
    Input to both models is the same: batch of images with shape: batch_size x 224 x 224 x 3
    """

    def __init__(self, num_classes, temperature, teacher_weight=2, alpha=1, depth_multiplier=1):
        super().__init__()
        self.img_width = 224
        self.img_height = 224
        self.rgb = 3

        self.num_classes = num_classes
        self.teacher_weight = teacher_weight
        self.alpha = alpha
        self.depth_multiplier = depth_multiplier
        self.layers = {}

        # self.temperature = Input(tensor=K.variable(temperature, dtype=tf.float32), shape=(1,), name='temperature')
        self.temperature = K.constant(value=temperature, dtype=tf.float32, name='temperature')

        self.input_shape = (self.img_width, self.img_height, self.rgb)
        self.layers['the_input'] = Input(shape=self.input_shape, name='the_input', dtype='float32')

        # first layer - full convolution
        x = self._full_convolution_layer()
        x = self._depthwise_separable_convolution_layer(x, 1, dw_strides=(1, 1), pw_filters=64)

        x = self._depthwise_separable_convolution_layer(x, 2, dw_strides=(2, 2), pw_filters=128)
        x = self._depthwise_separable_convolution_layer(x, 3, dw_strides=(1, 1), pw_filters=128)

        x = self._depthwise_separable_convolution_layer(x, 4, dw_strides=(2, 2), pw_filters=256)
        x = self._depthwise_separable_convolution_layer(x, 5, dw_strides=(1, 1), pw_filters=256)

        x = self._depthwise_separable_convolution_layer(x, 6, dw_strides=(2, 2), pw_filters=512)
        for i in range(5):
            x = self._depthwise_separable_convolution_layer(x, 7 + i, dw_strides=(1, 1), pw_filters=512)

        x = self._depthwise_separable_convolution_layer(x, 12, dw_strides=(2, 2), pw_filters=1024)
        x = self._depthwise_separable_convolution_layer(x, 13, dw_strides=(1, 1), pw_filters=1024)

        x = self._avg_pool_layer(x)

        # top layers:

        logits = Dense(
            units=self.num_classes,
            activation=None,
            kernel_initializer='he_normal'
        )(x)
        self.layers['logits'] = logits

        # normal softmax
        hard_preds = Activation('softmax')(logits)
        self.layers['softmax'] = hard_preds

        # softmax with temperature
        # for some reason we can't pass K.constant as args (keras explodes with strange error). So lambda must be used:
        scaled = Lambda(lambda x_in: tf.divide(x_in, self.temperature), output_shape=(self.num_classes,), name='scale', trainable=False)(logits)
        sotf_preds = Activation('softmax')(scaled)

        self.layers['output'] = concatenate([hard_preds, sotf_preds])

        # model for evaluation with NUM_CLASSES ouput
        self.evaluation_model = Model(inputs=self.layers['the_input'], outputs=self.layers['softmax'])
        # model for training with 2*NUM_CLASSES ouput
        self.model = Model(inputs=self.layers['the_input'], outputs=self.layers['output'])

    def _depthwise_separable_convolution_layer(self, inputs, idx, dw_strides, pw_filters):
        """
        :param inputs: previous layer (input to the depthwise separable convloution layer(
        :param idx: id of the layer
        :param dw_strides: depthwise convolution layers strides
        :param pw_filters: pointwise convolution filters
        :return: layer output (to use as input to next layer)
        """
        # depthwise convolution
        if dw_strides == (1, 1):
            # padding done by DepthwiseConv2D layer (output shape stays the same as input)
            x = inputs
            padding = 'same'
        else:
            # padding done manually; no padding in DepthwiseConv2D
            x = ZeroPadding2D(padding=((0, 1), (0, 1)), name='dw_pad_%d' % idx)(inputs)
            padding = 'valid'

        x = DepthwiseConv2D(
            kernel_size=(3, 3),
            strides=dw_strides,
            padding=padding,
            depth_multiplier=self.depth_multiplier,
            use_bias=False,
            name='dw_conv_%d' % idx
        )(x)
        x = BatchNormalization(name='dw_batch_%d' % idx)(x)
        x = ReLU(max_value=6.0, name='dw_relu_%d' % idx)(x)
        self.layers['dw_conv_%d' % idx] = x  # for tests

        # pointwise convolution
        x = Conv2D(
            filters=self._scale_filters(pw_filters),
            kernel_size=(1, 1),
            strides=(1, 1),
            padding='same',
            use_bias=False,
            name='pw_conv_%d' % idx
        )(x)
        x = BatchNormalization(name='pw_batch_%d' % idx)(x)
        x = ReLU(max_value=6.0, name='pw_relu_%d' % idx)(x)
        self.layers['pw_conv_%d' % idx] = x  # for tests

        return x

    def _full_convolution_layer(self):
        # x = ZeroPadding2D(padding=((0, 1), (0, 1)), name='zero_0')(self.layers['the_input'])
        x = Conv2D(
            filters=self._scale_filters(32),
            kernel_size=(3, 3),
            strides=(2, 2),
            padding='same',
            use_bias=False,
            name='conv_0'
        )(self.layers['the_input'])
        x = BatchNormalization(name='batch_0')(x)
        x = ReLU(max_value=6.0, name='relu_0')(x)
        self.layers['conv_0'] = x  # for tests
        return x

    def _avg_pool_layer(self, inputs):
        x = GlobalAveragePooling2D(name='avg_pool')(inputs)
        self.layers['avg_pool'] = x
        return x

    def _scale_filters(self, filters):
        return int(filters * self.alpha)

    def compile(self, lr=0.001):
        # you can't pass lambda to metrics (sic!),
        # so we need to define this function here to be able to access num_classes
        def training_accuracy(y_true, y_pred):
            # count accuracy only from output of regular softmax
            y_true = y_true[:, :self.num_classes]
            y_pred = y_pred[:, :self.num_classes]
            return K.mean(K.equal(K.argmax(y_true, axis=-1), K.argmax(y_pred, axis=-1)))

        optimizer = Adam(lr=lr)
        self.model.compile(
            loss=partial(
                _knowledge_distillation_loss,
                teacher_multiplier=self.temperature ** 2 * self.teacher_weight,
                num_classes=self.num_classes),
            optimizer=optimizer,
            metrics=[training_accuracy])
        self.evaluation_model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

    def get_layers_outputs(self):
        shapes = []
        for name, layer in self.layers.items():
            print('%s out shape: %s' % (name, str(layer.shape)))
            shapes.append(layer.shape.as_list()[1:4])
        return np.array(shapes)
