import numpy as np
import cv2
import os
import time
import logging
from abc import ABC, abstractmethod
from random import shuffle


# values from https://github.com/CSAILVision/places365/blob/master/train_placesCNN.py scaled to 0-255
MEAN = np.array([0.485, 0.456, 0.406]) * 255

LIMIT_PER_LABEL = 1000  # Set to None to disable limiting.


class AbstractBaseGenerator(ABC):
    def __init__(self, img_w, img_h, batch_size, subset_name):
        super().__init__()
        self.img_w = img_w
        self.img_h = img_h
        self.batch_size = batch_size
        self.subset_name = subset_name

    def generator(self):
        batch_images = []
        batch_targets = []

        start = time.time()
        while True:
            for img_path, target in self._inner_generator():
                batch_images.append(self._load_img(img_path))
                batch_targets.append(target)
                if len(batch_images) >= self.batch_size:
                    yield np.array(batch_images), np.array(batch_targets)
                    logging.debug('Creating a %s batch took: %f', self.subset_name, time.time() - start)
                    batch_images = []
                    batch_targets = []
                    start = time.time()

    @abstractmethod
    def count_steps_per_epoch(self):
        """Returns number of batches in one epoch"""
        pass

    @abstractmethod
    def _inner_generator(self):
        """Returns single sample consisting of image path and target for this image"""
        pass

    def _load_img(self, img_path):
        img = cv2.imread(img_path)
        if img.shape != (self.img_w, self.img_h, 3):
            img = cv2.resize(img, (self.img_w, self.img_h))

        img = np.array(img)
        return img - MEAN

    @staticmethod
    def _read_image_list(list_path, limit_per_label=None):
        with open(list_path) as f:
            seen_labels = {}
            for line in f:
                if not line:
                    continue

                image_rel_path, label = line.split(' ')
                if image_rel_path.startswith('/'):
                    image_rel_path = image_rel_path[1:]
                label = int(label)

                seen_labels[label] = seen_labels.get(label, 0) + 1
                if limit_per_label is None or seen_labels[label] <= limit_per_label:
                    yield (image_rel_path, label)


class DistilledDataGenerator(AbstractBaseGenerator):
    def __init__(
            self, img_w, img_h, batch_size, num_classes,
            list_path, images_path, distilled_path):
        super().__init__(img_w, img_h, batch_size, '\'distilled\'')
        self.num_classes = num_classes

        self.images_path = images_path
        self.distilled_path = distilled_path

        # Load the paths and targets into memory. Takes about 4 GB of RAM for
        # the whole Places365 training and validation datasets (in comparison to
        # doing this in _inner_generator, not in total).
        self.all_samples = []
        for rel_path, label in self._read_image_list(list_path, LIMIT_PER_LABEL):
            labels = np.zeros(self.num_classes)
            labels[label] = 1
            self.all_samples.append((
                os.path.join(self.images_path, rel_path),
                np.concatenate((labels, self._load_distilled(rel_path)))))

        print('Distilled dataset size: %d' % len(self.all_samples))
        assert len(self.all_samples) % self.batch_size == 0

    def count_steps_per_epoch(self):
        return len(self.all_samples) // self.batch_size

    def _inner_generator(self):
        shuffle(self.all_samples)
        yield from self.all_samples

    def _load_distilled(self, image_rel_path):
        """Load the targets from the teacher network."""
        filename = os.path.splitext(image_rel_path)[0] + '.npy'
        target_path = os.path.join(self.distilled_path, filename)
        return np.load(target_path)


class LabeledDataGenerator(AbstractBaseGenerator):
    def __init__(self, img_w, img_h, batch_size, subset_name, list_path, images_path, num_classes, mock_distilled=False):
        super().__init__(img_w, img_h, batch_size, subset_name)
        self.images_path = images_path
        self.list_path = list_path
        self.num_classes = num_classes
        self.mock_distilled = mock_distilled # whether to return mocked distilled target. Used in validation during training
        self.mock = np.zeros(self.num_classes)

        # Load paths and targets into memory.
        self.all_samples = []
        for rel_path, label in self._read_image_list(list_path, LIMIT_PER_LABEL):
            labels = np.zeros(self.num_classes)
            labels[label] = 1
            if self.mock_distilled:
                labels = np.concatenate((labels, self.mock))
            self.all_samples.append((
                os.path.join(self.images_path, rel_path), labels))

        print('Labeled dataset `%s` size: %d' % (subset_name, len(self.all_samples)))
        assert len(self.all_samples) % self.batch_size == 0

    def count_steps_per_epoch(self):
        return len(self.all_samples) // self.batch_size

    def _inner_generator(self):
        shuffle(self.all_samples)
        yield from self.all_samples
