Successfully built adf4dd625682
Successfully tagged mobilenet:latest
WARNING: IPv4 forwarding is disabled. Networking will not work.
Using TensorFlow backend.
Training with distillation
2019-01-26 10:59:15.059927: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:897] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2019-01-26 10:59:15.060285: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1392] Found device 0 with properties: 
name: GeForce GTX 1060 6GB major: 6 minor: 1 memoryClockRate(GHz): 1.7715
pciBusID: 0000:01:00.0
totalMemory: 5.93GiB freeMemory: 5.32GiB
2019-01-26 10:59:15.060305: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1471] Adding visible gpu devices: 0
2019-01-26 10:59:15.914092: I tensorflow/core/common_runtime/gpu/gpu_device.cc:952] Device interconnect StreamExecutor with strength 1 edge matrix:
2019-01-26 10:59:15.914164: I tensorflow/core/common_runtime/gpu/gpu_device.cc:958]      0 
2019-01-26 10:59:15.914187: I tensorflow/core/common_runtime/gpu/gpu_device.cc:971] 0:   N 
2019-01-26 10:59:15.914767: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1084] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 5084 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1060 6GB, pci bus id: 0000:01:00.0, compute capability: 6.1)
Distilled dataset size: 365000
Labeled dataset `validation` size: 36500
Epoch 1/15
 - 3021s - loss: 3.7758 - training_accuracy: 0.1723 - val_loss: 0.5599 - val_training_accuracy: 0.2646
Epoch 2/15
 - 3152s - loss: 3.7668 - training_accuracy: 0.3141 - val_loss: 0.5589 - val_training_accuracy: 0.3380
Epoch 3/15
 - 3122s - loss: 3.7636 - training_accuracy: 0.3762 - val_loss: 0.5595 - val_training_accuracy: 0.3869
Epoch 4/15
 - 3141s - loss: 3.7618 - training_accuracy: 0.4161 - val_loss: 0.5579 - val_training_accuracy: 0.4167
Epoch 5/15
 - 3124s - loss: 3.7605 - training_accuracy: 0.4462 - val_loss: 0.5584 - val_training_accuracy: 0.4228
Epoch 6/15
 - 3165s - loss: 3.7597 - training_accuracy: 0.4698 - val_loss: 0.5580 - val_training_accuracy: 0.4302
Epoch 7/15
 - 3118s - loss: 3.7589 - training_accuracy: 0.4909 - val_loss: 0.5578 - val_training_accuracy: 0.4452
Epoch 8/15
 - 3106s - loss: 3.7583 - training_accuracy: 0.5102 - val_loss: 0.5574 - val_training_accuracy: 0.4344
Epoch 9/15
 - 3129s - loss: 3.7578 - training_accuracy: 0.5269 - val_loss: 0.5576 - val_training_accuracy: 0.4573
Epoch 10/15
 - 3100s - loss: 3.7573 - training_accuracy: 0.5422 - val_loss: 0.5575 - val_training_accuracy: 0.4658
Epoch 11/15
 - 3102s - loss: 3.7570 - training_accuracy: 0.5569 - val_loss: 0.5578 - val_training_accuracy: 0.4639
Epoch 12/15
 - 3099s - loss: 3.7566 - training_accuracy: 0.5715 - val_loss: 0.5572 - val_training_accuracy: 0.4671
Epoch 13/15
 - 3099s - loss: 3.7563 - training_accuracy: 0.5855 - val_loss: 0.5578 - val_training_accuracy: 0.4657
Epoch 14/15
 - 3105s - loss: 3.7560 - training_accuracy: 0.5993 - val_loss: 0.5577 - val_training_accuracy: 0.4686
Epoch 15/15
 - 3138s - loss: 3.7557 - training_accuracy: 0.6109 - val_loss: 0.5575 - val_training_accuracy: 0.4705
