Successfully built adf4dd625682
Successfully tagged mobilenet:latest
WARNING: IPv4 forwarding is disabled. Networking will not work.
Using TensorFlow backend.
2019-01-25 05:05:50.914596: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:897] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2019-01-25 05:05:50.915245: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1392] Found device 0 with properties: 
name: GeForce GTX 1060 6GB major: 6 minor: 1 memoryClockRate(GHz): 1.7715
pciBusID: 0000:01:00.0
totalMemory: 5.93GiB freeMemory: 5.36GiB
2019-01-25 05:05:50.915275: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1471] Adding visible gpu devices: 0
2019-01-25 05:05:51.894904: I tensorflow/core/common_runtime/gpu/gpu_device.cc:952] Device interconnect StreamExecutor with strength 1 edge matrix:
2019-01-25 05:05:51.894948: I tensorflow/core/common_runtime/gpu/gpu_device.cc:958]      0 
2019-01-25 05:05:51.894958: I tensorflow/core/common_runtime/gpu/gpu_device.cc:971] 0:   N 
2019-01-25 05:05:51.895404: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1084] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 5126 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1060 6GB, pci bus id: 0000:01:00.0, compute capability: 6.1)
Distilled dataset size: 365000
Labeled dataset `validation` size: 36500
Epoch 1/15
 - 3088s - loss: 0.9340 - training_accuracy: 0.1749 - val_loss: 0.1500 - val_training_accuracy: 0.2537
Epoch 2/15
 - 3071s - loss: 0.9219 - training_accuracy: 0.3167 - val_loss: 0.1497 - val_training_accuracy: 0.3339
Epoch 3/15
 - 3070s - loss: 0.9177 - training_accuracy: 0.3786 - val_loss: 0.1477 - val_training_accuracy: 0.3826
Epoch 4/15
 - 3073s - loss: 0.9153 - training_accuracy: 0.4152 - val_loss: 0.1478 - val_training_accuracy: 0.4118
Epoch 5/15
 - 3071s - loss: 0.9137 - training_accuracy: 0.4452 - val_loss: 0.1471 - val_training_accuracy: 0.4231
Epoch 6/15
 - 3074s - loss: 0.9126 - training_accuracy: 0.4672 - val_loss: 0.1468 - val_training_accuracy: 0.4313
Epoch 7/15
 - 3086s - loss: 0.9116 - training_accuracy: 0.4863 - val_loss: 0.1476 - val_training_accuracy: 0.4415
Epoch 8/15
 - 3102s - loss: 0.9109 - training_accuracy: 0.5031 - val_loss: 0.1468 - val_training_accuracy: 0.4546
Epoch 9/15
 - 3095s - loss: 0.9103 - training_accuracy: 0.5200 - val_loss: 0.1469 - val_training_accuracy: 0.4555
Epoch 10/15
 - 3084s - loss: 0.9097 - training_accuracy: 0.5335 - val_loss: 0.1468 - val_training_accuracy: 0.4604
Epoch 11/15
 - 3080s - loss: 0.9092 - training_accuracy: 0.5478 - val_loss: 0.1471 - val_training_accuracy: 0.4641
Epoch 12/15
 - 3081s - loss: 0.9088 - training_accuracy: 0.5613 - val_loss: 0.1469 - val_training_accuracy: 0.4643
Epoch 13/15
 - 3079s - loss: 0.9085 - training_accuracy: 0.5733 - val_loss: 0.1467 - val_training_accuracy: 0.4685
Epoch 14/15
 - 3079s - loss: 0.9081 - training_accuracy: 0.5841 - val_loss: 0.1469 - val_training_accuracy: 0.4702
Epoch 15/15
 - 3113s - loss: 0.9078 - training_accuracy: 0.5956 - val_loss: 0.1469 - val_training_accuracy: 0.4681
