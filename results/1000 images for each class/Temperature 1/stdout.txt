Successfully built adf4dd625682
Successfully tagged mobilenet:latest
WARNING: IPv4 forwarding is disabled. Networking will not work.
Using TensorFlow backend.
2019-01-23 20:38:11.347961: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:897] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2019-01-23 20:38:11.348633: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1392] Found device 0 with properties: 
name: GeForce GTX 1060 6GB major: 6 minor: 1 memoryClockRate(GHz): 1.7715
pciBusID: 0000:01:00.0
totalMemory: 5.93GiB freeMemory: 5.33GiB
2019-01-23 20:38:11.348658: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1471] Adding visible gpu devices: 0
2019-01-23 20:38:12.219326: I tensorflow/core/common_runtime/gpu/gpu_device.cc:952] Device interconnect StreamExecutor with strength 1 edge matrix:
2019-01-23 20:38:12.219370: I tensorflow/core/common_runtime/gpu/gpu_device.cc:958]      0 
2019-01-23 20:38:12.219386: I tensorflow/core/common_runtime/gpu/gpu_device.cc:971] 0:   N 
2019-01-23 20:38:12.219790: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1084] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 5095 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1060 6GB, pci bus id: 0000:01:00.0, compute capability: 6.1)
Distilled dataset size: 365000
Labeled dataset `validation` size: 36500
Epoch 1/15
 - 3110s - loss: 0.0389 - training_accuracy: 0.1794 - val_loss: 0.0173 - val_training_accuracy: 0.2831
Epoch 2/15
 - 3075s - loss: 0.0303 - training_accuracy: 0.3314 - val_loss: 0.0167 - val_training_accuracy: 0.3629
Epoch 3/15
 - 3061s - loss: 0.0274 - training_accuracy: 0.3936 - val_loss: 0.0165 - val_training_accuracy: 0.3962
Epoch 4/15
 - 3044s - loss: 0.0257 - training_accuracy: 0.4335 - val_loss: 0.0166 - val_training_accuracy: 0.4142
Epoch 5/15
 - 3043s - loss: 0.0245 - training_accuracy: 0.4635 - val_loss: 0.0169 - val_training_accuracy: 0.4192
Epoch 6/15
 - 3040s - loss: 0.0235 - training_accuracy: 0.4910 - val_loss: 0.0167 - val_training_accuracy: 0.4356
Epoch 7/15
 - 3042s - loss: 0.0227 - training_accuracy: 0.5121 - val_loss: 0.0169 - val_training_accuracy: 0.4379
Epoch 8/15
 - 3040s - loss: 0.0221 - training_accuracy: 0.5330 - val_loss: 0.0171 - val_training_accuracy: 0.4454
Epoch 9/15
 - 3040s - loss: 0.0215 - training_accuracy: 0.5511 - val_loss: 0.0170 - val_training_accuracy: 0.4470
Epoch 10/15
 - 3041s - loss: 0.0210 - training_accuracy: 0.5688 - val_loss: 0.0171 - val_training_accuracy: 0.4493
Epoch 11/15
 - 3040s - loss: 0.0205 - training_accuracy: 0.5849 - val_loss: 0.0173 - val_training_accuracy: 0.4483
Epoch 12/15
 - 3043s - loss: 0.0201 - training_accuracy: 0.6004 - val_loss: 0.0174 - val_training_accuracy: 0.4505
Epoch 13/15
 - 3041s - loss: 0.0198 - training_accuracy: 0.6142 - val_loss: 0.0173 - val_training_accuracy: 0.4520
Epoch 14/15
 - 3044s - loss: 0.0195 - training_accuracy: 0.6281 - val_loss: 0.0174 - val_training_accuracy: 0.4516
Epoch 15/15
 - 3043s - loss: 0.0192 - training_accuracy: 0.6396 - val_loss: 0.0175 - val_training_accuracy: 0.4550
